function problem3(inventory)
{

    // sort inventory according to car_model name in asce order
    if(inventory != undefined && inventory.length != 0 ){
    inventory.sort(function(a,b){
        if(a.car_model.toLowerCase() < b.car_model.toLowerCase())
            return -1
        if(a.car_model.toLowerCase() > b.car_model.toLowerCase())
            return 1;
        return 0;
    })
    return inventory;
}
else return "There is not data for sort inside inventory"
}
module.exports = problem3