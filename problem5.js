function problem5(years)
{
    let yearLessThen2000 = []
    if( years === undefined || years.length == 0) 
    return "No data found"
    for(let i = 0;i<years.length ; i++)
    {
        if(years[i] < 2000)
        yearLessThen2000.push(years[i])
    }
    return  yearLessThen2000;
}
module.exports = problem5