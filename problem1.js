function problem1(inventory,value) {
    if (inventory != undefined && inventory.length != 0 && value != undefined ) {
        for (let i = 0; i < inventory.length; i++) {
            if (inventory[i].id == value) {
                return inventory[i];
            }
        }

    }
    return [];

}
module.exports = problem1
